<?php

namespace WS\Library\ObjectExtensions\Timestamp\Model;

/**
 * Trait CreatedTimestampMethodsTrait
 *
 * @author Kévin Tourret
 */
trait CreatedTimestampMethodsTrait
{
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt ? : $this->createdAt = new \DateTime();
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): static
    {
        if (null !== $createdAt) {
            $this->createdAt = $createdAt;
        }

        return $this;
    }
}