<?php

namespace WS\Library\ObjectExtensions\Timestamp\Model;

/**
 * Trait UpdatedTimestampMethodsTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedTimestampMethodsTrait
{
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        if (null !== $updatedAt) {
            $this->updatedAt = $updatedAt;
        }

        return $this;
    }

}