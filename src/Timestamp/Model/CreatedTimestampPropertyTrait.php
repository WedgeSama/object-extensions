<?php

namespace WS\Library\ObjectExtensions\Timestamp\Model;

/**
 * Trait CreatedTimestampPropertyTrait
 *
 * @author Kévin Tourret
 */
trait CreatedTimestampPropertyTrait
{
    protected ?\DateTimeInterface $createdAt = null;
}