<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Timestamp\Model;

/**
 * Interface CreatedTimestampInterface
 *
 * @author Benjamin Georgeault
 */
interface CreatedTimestampInterface
{
    public function getCreatedAt(): \DateTimeInterface;

    public function setCreatedAt(?\DateTimeInterface $createdAt): static;
}
