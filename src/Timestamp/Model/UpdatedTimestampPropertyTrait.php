<?php

namespace WS\Library\ObjectExtensions\Timestamp\Model;

/**
 * Trait UpdatedTimestampPropertyTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedTimestampPropertyTrait
{
    protected ?\DateTimeInterface $updatedAt = null;

}