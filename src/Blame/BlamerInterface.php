<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Blame;

use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Interface BlamerInterface
 *
 * @author Benjamin Georgeault
 */
interface BlamerInterface
{
    public function applyCreatedBy(CreatedBlameInterface $blameObject): void;

    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void;

    /**
     * Check if the given object is supported by this Blamer for the logged User.
     */
    public function support(string $blameClass): bool;
}
