<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Trait BlameTrait
 *
 * @author Benjamin Georgeault
 * @see BlameInterface
 */
trait BlameTrait // implements BlameInterface
{
    use CreatedBlameTrait;
    use UpdatedBlameTrait;
}
