<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Interface CreatedBlameInterface
 *
 * @author Benjamin Georgeault
 *
 * @template T of object
 */
interface CreatedBlameInterface
{
    /**
     * @psalm-return T|null
     */
    public function getCreatedBy(): ?object;

    /**
     * @psalm-param T|null $createdBy
     */
    public function setCreatedBy(?object $createdBy): static;
}
