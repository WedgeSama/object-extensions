<?php

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Trait UpdatedBlameMethodsTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedBlameMethodsTrait
{
    public function getUpdatedBy(): ?object
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?object $updatedBy): static
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}