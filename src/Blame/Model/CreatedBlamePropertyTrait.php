<?php

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Trait CreatedBlamePropertyTrait
 *
 * @author Kévin Tourret
 */
trait CreatedBlamePropertyTrait
{
    protected ?object $createdBy = null;
}