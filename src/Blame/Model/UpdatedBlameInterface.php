<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Interface UpdatedBlameInterface
 *
 * @author Benjamin Georgeault
 *
 * @template T of object
 */
interface UpdatedBlameInterface
{
    /**
     * @psalm-return T|null
     */
    public function getUpdatedBy(): ?object;

    /**
     * @psalm-param T|null $updatedBy
     */
    public function setUpdatedBy(?object $updatedBy): static;
}
