<?php

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Trait UpdatedBlamePropertyTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedBlamePropertyTrait
{
    protected ?object $updatedBy = null;
}