<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Interface BlameInterface
 *
 * @author Benjamin Georgeault
 *
 * @template T of object
 * @template-implements CreatedBlameInterface<T>
 * @template-implements UpdatedBlameInterface<T>
 */
interface BlameInterface extends CreatedBlameInterface, UpdatedBlameInterface
{
}
