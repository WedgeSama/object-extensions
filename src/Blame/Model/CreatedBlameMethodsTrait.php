<?php

namespace WS\Library\ObjectExtensions\Blame\Model;

/**
 * Trait CreatedBlameMethodsTrait
 *
 * @author Kévin Tourret
 */
trait CreatedBlameMethodsTrait
{
    public function getCreatedBy(): ?object
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?object $createdBy): static
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}