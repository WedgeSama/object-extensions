<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Blame;

use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Class Blamer
 *
 * @author Benjamin Georgeault
 */
class Blamer implements BlamerInterface
{
    public function __construct(
        private readonly object $user,
    ) {}

    public function applyCreatedBy(CreatedBlameInterface $blameObject): void
    {
        $blameObject->setCreatedBy($this->user);
    }

    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void
    {
        $blameObject->setUpdatedBy($this->user);
    }

    public function support(string $blameClass): bool
    {
        return is_a($this->user, $blameClass);
    }
}
