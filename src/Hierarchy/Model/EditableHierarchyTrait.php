<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Hierarchy\Model;

/**
 * Trait EditableHierarchyTrait
 *
 * @author Benjamin Georgeault
 * @see EditableHierarchyInterface
 */
trait EditableHierarchyTrait // implements EditableHierarchyInterface
{
    use HierarchyTrait;

    public function setParent(?EditableHierarchyInterface $parent): static
    {
        $this->parent = $parent;

        if (null !== $parent && !$parent->hasChild($this) && $parent instanceof EditableHierarchyInterface) {
            $parent->addChild($this);
        }

        return $this;
    }

    public function addChild(EditableHierarchyInterface $child): static
    {
        if ($this->hasChild($child)) {
            throw new \InvalidArgumentException(sprintf('Child already in the collection.'));
        }

        $this->children[] = $child;

        if ($this !== $child->getParent() && $child instanceof EditableHierarchyInterface) {
            $child->setParent($this);
        }

        return $this;
    }

    public function remove(EditableHierarchyInterface $child): static
    {
        if (false === $key = array_search($child, $this->children)) {
            throw new \OutOfBoundsException(sprintf('Child not found in the collection.'));
        }

        unset($this->children[$key]);

        return $this;
    }
}
