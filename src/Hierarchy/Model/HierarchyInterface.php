<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Hierarchy\Model;

/**
 * Interface HierarchyInterface
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of HierarchyInterface
 * @template-implements \IteratorAggregate<TKey, TValue>
 */
interface HierarchyInterface extends \IteratorAggregate, \Countable
{
    /**
     * @psalm-return TValue|null
     */
    public function getParent(): ?HierarchyInterface;

    /**
     * @psalm-return iterable<TKey, TValue>
     * @return iterable<HierarchyInterface>
     */
    public function getChildren(): iterable;

    /**
     * @psalm-param TValue $child
     * @param HierarchyInterface $child
     * @return bool
     */
    public function hasChild(HierarchyInterface $child): bool;
}
