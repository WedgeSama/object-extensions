<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Hierarchy\Model;

/**
 * Interface EditableHierarchyInterface
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of EditableHierarchyInterface
 * @template-implements HierarchyInterface<TKey, TValue>
 */
interface EditableHierarchyInterface extends HierarchyInterface
{
    /**
     * @psalm-param TValue|null $parent
     */
    public function setParent(?EditableHierarchyInterface $parent): static;

    /**
     * @psalm-param TValue $child
     */
    public function addChild(EditableHierarchyInterface $child): static;

    /**
     * @psalm-param TValue $child
     */
    public function remove(EditableHierarchyInterface $child): static;
}
