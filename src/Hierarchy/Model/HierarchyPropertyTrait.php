<?php

namespace WS\Library\ObjectExtensions\Hierarchy\Model;

/**
 * Trait HierarchyPropertyTrait
 *
 * @author Kévin Tourret
 */
trait HierarchyPropertyTrait
{
    protected ?self $parent;

    /**
     * @var Array<static>
     */
    protected iterable $children = [];
}