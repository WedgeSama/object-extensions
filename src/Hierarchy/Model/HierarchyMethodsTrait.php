<?php

namespace WS\Library\ObjectExtensions\Hierarchy\Model;

/**
 * Trait HierarchyMethodsTrait
 *
 * @author Kévin Tourret
 */
trait HierarchyMethodsTrait
{
    public function getIterator(): iterable
    {
        return new \ArrayIterator($this->children);
    }

    public function count(): int
    {
        return count($this->children);
    }

    public function getParent(): ?HierarchyInterface
    {
        return $this->parent;
    }

    public function getChildren(): iterable
    {
        return $this->children;
    }

    public function hasChild(HierarchyInterface $child): bool
    {
        return in_array($child, $this->children);
    }
}