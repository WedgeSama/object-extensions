<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\SerializeLog;

use WS\Library\ObjectExtensions\SerializeLog\Model\SerializeLogInterface;
use WS\Library\ObjectExtensions\SerializeLog\Model\State;
use WS\Library\ObjectExtensions\SerializeLog\Model\StateInterface;

/**
 * Class AbstractLogger
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractLogger implements LoggerInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly string $stateClass = State::class,
        private readonly ?ContextLoaderInterface $contextLoader = null,
    ) {
        if (!is_subclass_of($this->stateClass, StateInterface::class)) {
            throw new \InvalidArgumentException(sprintf('The state class must implement %s', StateInterface::class));
        }
    }

    public function log(SerializeLogInterface $object, string $message = ''): StateInterface
    {
        return $this->doState($object, $message);
    }

    abstract protected function getClass(SerializeLogInterface $object): string;

    abstract protected function getIdentifier(SerializeLogInterface $object): mixed;

    private function doState(SerializeLogInterface $object, string $message): StateInterface
    {
        $class = $this->stateClass;
        /** @var StateInterface $state */
        $state = new $class();

        $state
            ->setClass($this->getClass($object))
            ->setIdentifier($this->getIdentifier($object))
            ->setMessage($message)
            ->setData($this->serializer->serialize($object, $object::getSerializeLogGroups()))
        ;

        if ($this->contextLoader) {
            $state->setContext($this->contextLoader->getContext());
        }

        return $state;
    }
}
