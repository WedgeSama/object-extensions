<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\SerializeLog;

/**
 * Class ChainContextLoader
 *
 * @author Benjamin Georgeault
 */
class ChainContextLoader implements ContextLoaderInterface
{
    /**
     * @var Array<AbstractSupportableContextLoader>
     */
    private array $contextLoaders;

    private array|false|null $context = false;

    /**
     * ChainContextLoader constructor.
     * @param Array<AbstractSupportableContextLoader> $contextLoaders
     */
    public function __construct(array $contextLoaders)
    {
        foreach ($contextLoaders as $contextLoader) {
            if (!$contextLoader instanceof AbstractSupportableContextLoader) {
                throw new \InvalidArgumentException(sprintf(
                    'For %s, given contexts must all be instance of %s.',
                    self::class,
                    AbstractSupportableContextLoader::class
                ));
            }
        }

        $this->contextLoaders = $contextLoaders;
    }

    public function getContext(): ?array
    {
        if (false === $this->context) {
            $this->context = $this->loadContext();
        }

        return $this->context;
    }

    private function loadContext(): ?array
    {
        foreach ($this->contextLoaders as $contextLoader) {
            if (
                $contextLoader->support()
                && null !== $context = $contextLoader->getContext()
            ) {
                return $context;
            }
        }

        return null;
    }
}
