<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\SerializeLog\Model;

use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameTrait;
use WS\Library\ObjectExtensions\Timestamp\Model\CreatedTimestampTrait;

/**
 * Class SerializeLogState
 *
 * @author Benjamin Georgeault
 */
class State implements StateInterface
{
    use CreatedBlameTrait;
    use CreatedTimestampTrait;

    private ?int $id = null;

    private string $class;

    private mixed $identifier;

    private string $message;

    private array $data;

    private ?array $context = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function setClass(string $class): StateInterface
    {
        $this->class = $class;
        return $this;
    }

    public function getIdentifier(): mixed
    {
        return $this->identifier;
    }

    public function setIdentifier(mixed $identifier): StateInterface
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): StateInterface
    {
        $this->message = $message;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): StateInterface
    {
        $this->data = $data;
        return $this;
    }

    public function getContext(): ?array
    {
        return $this->context;
    }

    public function setContext(?array $context): StateInterface
    {
        $this->context = $context;
        return $this;
    }
}
