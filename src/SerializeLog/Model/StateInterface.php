<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\SerializeLog\Model;

use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\CreatedTimestampInterface;

/**
 * Interface StateInterface
 *
 * @author Benjamin Georgeault
 */
interface StateInterface extends CreatedBlameInterface, CreatedTimestampInterface
{
    public function getClass(): string;
    public function setClass(string $class): self;
    public function getIdentifier(): mixed;
    public function setIdentifier(mixed $identifier): self;
    public function getMessage(): string;
    public function setMessage(string $message): self;
    public function getData(): array;
    public function setData(array $data): self;
    public function getContext(): ?array;
    public function setContext(?array $context): self;
}
