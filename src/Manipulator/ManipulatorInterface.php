<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Manipulator;

/**
 * Interface ManipulatorInterface
 *
 * @author Benjamin Georgeault
 */
interface ManipulatorInterface
{
    /**
     * Update data from sources into target property by property.
     * (take only properties found on both sides)
     *
     * @param object $target
     * @param object|array ...$sources
     * @return object Return the target object.
     */
    public function updater(object $target, ...$sources): object;

    /**
     * Update data from sources into target property by property.
     * (take only properties listed in $props)
     *
     * @param array $props
     * @param object $target
     * @param object|array ...$sources
     * @return object Return the target object.
     */
    public function selectedUpdater(array $props, object $target, ...$sources): object;

    /**
     * Update data from sources into target property by property.
     * (take only properties found on both sides and not present in $ignoredProps)
     *
     * @param array $ignoredProps
     * @param object $target
     * @param object|array ...$sources
     * @return object Return the target object.
     */
    public function ignoredUpdater(array $ignoredProps, object $target, ...$sources): object;

    /**
     * Force hydratation of the property on target. Even private property without setter.
     *
     * @param object $target
     * @param string $property
     * @param mixed $value
     * @return object Return the target object.
     */
    public function hydrate(object $target, string $property, mixed $value): object;
}
