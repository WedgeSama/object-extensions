<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Disable\Model;

/**
 * Trait CanDisableMethodsTrait
 *
 * @author Benjamin Georgeault
 */
trait CanDisableMethodsTrait
{
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): static
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function disable(): static
    {
        return $this->setDisabled(true);
    }

    public function enable(): static
    {
        return $this->setDisabled(false);
    }
}
