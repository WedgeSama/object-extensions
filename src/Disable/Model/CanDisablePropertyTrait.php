<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Disable\Model;

/**
 * Trait CanDisablePropertyTrait
 *
 * @author Benjamin Georgeault
 */
trait CanDisablePropertyTrait
{
    protected bool $disabled = false;
}
