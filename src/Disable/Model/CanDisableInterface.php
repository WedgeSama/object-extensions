<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Disable\Model;

/**
 * Interface CanDisableInterface
 *
 * @author Benjamin Georgeault
 */
interface CanDisableInterface
{
    public function isDisabled(): bool;

    public function setDisabled(bool $disabled): static;

    public function disable(): static;

    public function enable(): static;
}
