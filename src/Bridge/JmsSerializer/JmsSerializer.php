<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\JmsSerializer;

use WS\Library\ObjectExtensions\SerializeLog\SerializerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;

/**
 * Class JmsSerializer
 *
 * @author Benjamin Georgeault
 */
readonly class JmsSerializer implements SerializerInterface
{
    public function __construct(
        private JmsSerializerInterface $jmsSerializer,
    ) {}

    public function serialize(object $object, array $groups = []): array
    {
        $context = new SerializationContext();
        $context->setGroups($groups);

        return json_decode(
            $this->jmsSerializer->serialize($object, 'json', $context),
            true
        );
    }
}
