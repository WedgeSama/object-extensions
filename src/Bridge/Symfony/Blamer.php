<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony;

use WS\Library\ObjectExtensions\Blame\BlamerInterface;
use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\UpdatedBlameInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class Blamer
 *
 * @author Benjamin Georgeault
 */
class Blamer implements BlamerInterface
{
    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        private readonly string $userClass,
        private readonly array $namespaces,
    ) {}

    public function applyCreatedBy(CreatedBlameInterface $blameObject): void
    {
        if (null !== $user = $this->getUser()) {
            $blameObject->setCreatedBy($user);
        }
    }

    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void
    {
        if (null !== $user = $this->getUser()) {
            $blameObject->setUpdatedBy($user);
        }
    }

    public function support(string $blameClass): bool
    {
        if (null !== $this->getUser()) {
            foreach ($this->namespaces as $namespace) {
                if (strstr($blameClass, $namespace)) {
                    return true;
                }
            }
        }

        return false;
    }

    private function getUser(): ?object
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        $user = $token->getUser();
        if ($user instanceof $this->userClass) {
            return $user;
        }

        return null;
    }
}
