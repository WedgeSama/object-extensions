<?php
/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony;

use Symfony\Component\HttpFoundation\RequestStack;
use WS\Library\ObjectExtensions\Translation\LocalizerInterface;

/**
 * Class Localizer
 *
 * @author Benjamin Georgeault
 */
class Localizer implements LocalizerInterface
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly string $cmdLocaleFallback,
    ) {}

    public function getCurrentLocale(): string
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request ? $request->getLocale() : $this->cmdLocaleFallback;
    }

    public function getFallbackLocale(): string
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request ? $request->getDefaultLocale() : $this->cmdLocaleFallback;
    }
}
