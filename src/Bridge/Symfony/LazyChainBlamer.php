<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony;

use WS\Library\ObjectExtensions\Blame\BlamerInterface;
use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\UpdatedBlameInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LazyChainBlamer
 *
 * @author Benjamin Georgeault
 */
class LazyChainBlamer implements BlamerInterface
{
    /**
     * @var Array<BlamerInterface>
     */
    private array $blamers = [];

    /**
     * @param string[] $blamerIds
     */
    public function __construct(
        private readonly ContainerInterface $container,
        private array $blamerIds,
    ) {}

    public function applyCreatedBy(CreatedBlameInterface $blameObject): void
    {
        if (null !== $blamer = $this->getBlamer(get_class($blameObject))) {
            $blamer->applyCreatedBy($blameObject);
        }
    }

    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void
    {
        if (null !== $blamer = $this->getBlamer(get_class($blameObject))) {
            $blamer->applyUpdatedBy($blameObject);
        }
    }

    public function support(string $blameClass): bool
    {
        return null !== $this->getBlamer($blameClass);
    }

    private function getBlamer(string $class): ?BlamerInterface
    {
        foreach ($this->blamers as $blamer) {
            if ($blamer->support($class)) {
                return $blamer;
            }
        }

        // Lazy loading Blamers.
        while (null !== $id = array_shift($this->blamerIds)) {
            $blamer = $this->container->get($id);

            if (!$blamer instanceof BlamerInterface) {
                throw new \InvalidArgumentException(sprintf(
                    'The service "%s" is not an instance of "%s".',
                    $id,
                    BlamerInterface::class
                ));
            }

            if ($blamer instanceof LazyChainBlamer) {
                throw new \InvalidArgumentException(sprintf(
                    'The service "%s" is a "%s" instance, it cannot be add to another instance of "%s".',
                    $id,
                    LazyChainBlamer::class,
                    LazyChainBlamer::class
                ));
            }

            $this->blamers[] = $blamer;
            if ($blamer->support($class)) {
                return $blamer;
            }
        }

        return null;
    }
}
