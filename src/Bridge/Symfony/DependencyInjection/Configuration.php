<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony\DependencyInjection;

use WS\Library\ObjectExtensions\Bridge\Symfony\Localizer;
use WS\Library\ObjectExtensions\SerializeLog\Model\State;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('ws_object_extensions');

        $treeBuilder->getRootNode()
            ->children()
                ->append($this->blameNode())
                ->append($this->hierarchyNode())
                ->append($this->slugNode())
                ->append($this->timestampNode())
                ->append($this->translationNode())
                ->append($this->serializeLogNode())
                ->append($this->manipulatorNode())
                ->append($this->disableNode())
            ->end()
        ;

        return $treeBuilder;
    }

    private function blameNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('blame');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->arrayNode('users')
                    ->useAttributeAsKey('class')
                    ->arrayPrototype()
                        ->beforeNormalization()->castToArray()->end()
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }

    private function hierarchyNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('hierarchy');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    private function slugNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('slug');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->scalarNode('delimiter')
                    ->cannotBeEmpty()
                    ->defaultValue('-')
                ->end()
                ->booleanNode('entropy')
                    ->defaultFalse()
                ->end()
                ->booleanNode('on_pre_update')
                    ->defaultFalse()
                ->end()
                ->scalarNode('hierarchy_delimiter')
                    ->cannotBeEmpty()
                    ->defaultValue('/')
                ->end()
            ->end()
        ;

        return $node;
    }

    private function timestampNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('timestamp');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    private function translationNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('translation');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->scalarNode('localizer')
                    ->cannotBeEmpty()
                    ->defaultValue(Localizer::class)
                ->end()
                ->scalarNode('cmd_locale_fallback')
                    ->cannotBeEmpty()
                    ->defaultValue('en')
                ->end()
            ->end()
        ;

        return $node;
    }

    private function serializeLogNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('serialize_log');

        $node = $treeBuilder->getRootNode()
            ->canBeEnabled()
            ->children()
                ->enumNode('serializer')
                    ->values(['symfony', 'jms'])
                    ->defaultValue('jms')
                ->end()
                ->scalarNode('state_class')
                    ->cannotBeEmpty()
                    ->defaultValue(State::class)
                ->end()
                ->scalarNode('model_manager_name')
                    ->cannotBeEmpty()
                    ->defaultValue('default')
                ->end()
            ->end()
        ;

        return $node;
    }

    private function manipulatorNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('manipulator');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    private function disableNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('disable');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }
}
