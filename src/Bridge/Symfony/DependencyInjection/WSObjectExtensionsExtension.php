<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony\DependencyInjection;

use Doctrine\ORM\Events;
use WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping\BlameSubscriber;
use WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\SlugSubscriber;
use WS\Library\ObjectExtensions\Bridge\JmsSerializer\JmsSerializer;
use WS\Library\ObjectExtensions\Bridge\Symfony\DoctrineBlamer;
use WS\Library\ObjectExtensions\Bridge\Symfony\Localizer;
use WS\Library\ObjectExtensions\Bridge\Symfony\SerializeLog\Serializer;
use WS\Library\ObjectExtensions\SerializeLog\AbstractLogger;
use WS\Library\ObjectExtensions\SerializeLog\ContextLoaderInterface;
use WS\Library\ObjectExtensions\SerializeLog\Model\State;
use WS\Library\ObjectExtensions\SerializeLog\Model\StateInterface;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;
use WS\Library\ObjectExtensions\Translation\LocalizerInterface;

/**
 * Class WSObjectExtensionsExtension
 *
 * @author Benjamin Georgeault
 */
class WSObjectExtensionsExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        if ($config['blame']['enabled']) {
            $loader->load('blame.yaml');

            foreach ($config['blame']['users'] as $userClass => $namespaces) {
                // Mapping
                $mappingDefinition = new Definition(BlameSubscriber::class, [
                    $userClass,
                    $namespaces,
                ]);

                $mappingDefinition
                    ->setPublic(false)
                    ->addTag('doctrine.event_listener', [
                        'event' => Events::loadClassMetadata,
                    ])
                ;

                $container->setDefinition(BlameSubscriber::class . '.' . $userClass, $mappingDefinition);

                // Blamer
                $blamerDefinition = new Definition(DoctrineBlamer::class, [
                    new Reference('security.token_storage'),
                    new Reference('doctrine.orm.entity_manager'),
                    $userClass,
                    $namespaces,
                ]);

                $blamerDefinition
                    ->setPublic(true)
                    ->addTag('ws_object_extensions.blamer')
                ;

                $container->setDefinition(DoctrineBlamer::class . '.' . $userClass, $blamerDefinition);
            }
        }

        if ($config['hierarchy']['enabled']) {
            $loader->load('hierarchy.yaml');
        }

        if ($config['slug']['enabled']) {
            $loader->load('slug.yaml');
            $container->setParameter('ws_object_extensions.slug.delimiter', $config['slug']['delimiter']);
            $container->setParameter('ws_object_extensions.slug.on_pre_update', $config['slug']['on_pre_update']);
            $container->setParameter('ws_object_extensions.slug.entropy', $config['slug']['entropy']);

            if ($config['slug']['on_pre_update']) {
                $container->getDefinition(SlugSubscriber::class)
                    ->addTag('doctrine.event_listener', [
                        'event' => 'preUpdate',
                    ])
                ;
            }

            if ($config['hierarchy']['enabled']) {
                $loader->load('slug_hierarchy.yaml');
                $container->setParameter('ws_object_extensions.slug.hierarchy_delimiter', $config['slug']['hierarchy_delimiter']);
            }
        }

        if ($config['timestamp']['enabled']) {
            $loader->load('timestamp.yaml');
        }

        if ($config['translation']['enabled']) {
            $loader->load('translation.yaml');

            $container->setAlias(LocalizerInterface::class, $config['translation']['localizer']);
            $container->getDefinition(Localizer::class)
                ->setArgument('$cmdLocaleFallback', $config['translation']['cmd_locale_fallback'])
            ;
        }

        if ($config['serialize_log']['enabled']) {
            $loader->load('serialize_log.yaml');
            $container->setParameter(
                'ws_object_extensions.serialize_log.model_manager_name',
                $config['serialize_log']['model_manager_name']
            );

            $abstractLoggerDefinition = $container->getDefinition(AbstractLogger::class);

            try {
                $stateRef = new \ReflectionClass($config['serialize_log']['state_class']);
                if (!$stateRef->implementsInterface(StateInterface::class)) {
                    throw new InvalidArgumentException(sprintf(
                        'Given state_class "%s" for serialize_log has to implement "%s" interface.',
                        $config['serialize_log']['state_class'],
                        StateInterface::class
                    ));
                }
            } catch (\ReflectionException $e) {
                throw new InvalidArgumentException(sprintf(
                    'Given state_class "%s" for serialize_log does not exist.',
                    $config['serialize_log']['state_class']
                ));
            }

            if (State::class === $config['serialize_log']['state_class']) {
                $container->setParameter('ws_object_extensions.serialize_log.use_internal_entity', true);
            }

            if ('symfony' === $config['serialize_log']['serializer']) {
                if (!interface_exists('Symfony\\Component\\Serializer\\SerializerInterface')) {
                    throw new InvalidArgumentException(
                        'WS serialize_log configured to use serializer from Symfony but symfony/serializer dependency is missing.'
                    );
                }

                $container->setDefinition(Serializer::class, new Definition(Serializer::class))
                    ->setPublic(false)
                    ->setArgument(0, new Reference(SymfonySerializerInterface::class))
                ;

                $abstractLoggerDefinition->setArgument(0, new Reference(Serializer::class));
            } elseif ('jms' === $config['serialize_log']['serializer']) {
                if (!class_exists('JMS\\SerializerBundle\\JMSSerializerBundle')) {
                    throw new InvalidArgumentException(
                        'WS serialize_log configured to use serializer from JMS but jms/serializer-bundle dependency is missing.'
                    );
                }

                $container->setDefinition(JmsSerializer::class, new Definition(JmsSerializer::class))
                    ->setPublic(false)
                    ->setArgument(0, new Reference(JmsSerializerInterface::class))
                ;

                $abstractLoggerDefinition->setArgument(0, new Reference(JmsSerializer::class));
            }

            $abstractLoggerDefinition->setArgument(1, $config['serialize_log']['state_class']);

            $abstractLoggerDefinition->setArgument(2, new Reference(ContextLoaderInterface::class));
        }

        if ($config['manipulator']['enabled']) {
            $loader->load('manipulator.yaml');
        }

        if ($config['disable']['enabled']) {
            $loader->load('disable.yaml');
        }
    }

    public function getAlias(): string
    {
        return 'ws_object_extensions';
    }
}
