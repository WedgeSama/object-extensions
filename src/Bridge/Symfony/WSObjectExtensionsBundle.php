<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use WS\Library\ObjectExtensions\Bridge\Symfony\DependencyInjection\Compiler\BlamerPass;
use WS\Library\ObjectExtensions\Bridge\Symfony\DependencyInjection\WSObjectExtensionsExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WSObjectExtensionsBundle
 *
 * @author Benjamin Georgeault
 */
class WSObjectExtensionsBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new BlamerPass());

        if (class_exists('Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass')) {
            $container->addCompilerPass(DoctrineOrmMappingsPass::createXmlMappingDriver([
                realpath(__DIR__.'/../Doctrine/ORM/SerializeLog/mapping') => 'WS\Library\ObjectExtensions\SerializeLog\Model',
            ], ['ws_object_extensions.serialize_log.model_manager_name'], 'ws_object_extensions.serialize_log.use_internal_entity'));
        }
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new WSObjectExtensionsExtension();
    }
}
