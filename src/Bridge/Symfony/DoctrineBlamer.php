<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony;

use Doctrine\Persistence\Mapping\MappingException;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DoctrineBlamer
 *
 * @author Benjamin Georgeault
 */
class DoctrineBlamer extends Blamer
{
    public function __construct(
        TokenStorageInterface $tokenStorage,
        private readonly ObjectManager $om,
        string $userClass,
        array $namespaces
    ) {
        parent::__construct($tokenStorage, $userClass, $namespaces);
    }

    public function support(string $blameClass): bool
    {
        return parent::support($this->cleanClass($blameClass));
    }

    private function cleanClass(string $class): string
    {
        try {
            return $this->om->getClassMetadata($class)->getName();
        } catch (MappingException) {
            return $class;
        }
    }
}
