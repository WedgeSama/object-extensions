<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony\SerializeLog;

use WS\Library\ObjectExtensions\SerializeLog\AbstractSupportableContextLoader;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RequestContextLoader
 *
 * @author Benjamin Georgeault
 */
class RequestContextLoader extends AbstractSupportableContextLoader
{
    public function __construct(
        private readonly RequestStack $requestStack,
    ) {}

    public function support(): bool
    {
        return null !== $this->requestStack->getCurrentRequest();
    }

    public function getContext(): ?array
    {
        $request = $this->requestStack->getCurrentRequest();

        return [
            'route' => $request->attributes->get('_route'),
            'route_params' => $request->attributes->get('_route_params'),
            'controller' => $request->attributes->get('_controller'),
            'uri' => $request->getUri(),
        ];
    }
}
