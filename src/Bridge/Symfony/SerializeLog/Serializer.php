<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Symfony\SerializeLog;

use WS\Library\ObjectExtensions\SerializeLog\SerializerInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

/**
 * Class Serializer
 *
 * @author Benjamin Georgeault
 */
class Serializer implements SerializerInterface
{
    public function __construct(
        private readonly SymfonySerializerInterface $symfonySerializer,
    ) {}

    public function serialize($object, array $groups = []): array
    {
        return json_decode(
            $this->symfonySerializer->serialize($object, 'json', ['groups' => $groups]),
            true
        );
    }
}
