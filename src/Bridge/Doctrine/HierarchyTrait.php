<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface;

/**
 * Trait HierarchyTrait
 *
 * @author Benjamin Georgeault
 * @see HierarchyInterface
 */
trait HierarchyTrait // implements HierarchyInterface
{
    protected ?HierarchyInterface $parent;

    /**
     * @var ?Collection<static>
     */
    protected ?Collection $children = null;

    public function getIterator(): \Traversable
    {
        return $this->getChildren()->getIterator();
    }

    public function count(): int
    {
        return $this->getChildren()->count();
    }

    public function getParent(): ?HierarchyInterface
    {
        return $this->parent;
    }

    /**
     * @return Collection<static>
     */
    public function getChildren(): Collection
    {
        return $this->children ? : $this->children = new ArrayCollection();
    }

    public function hasChild(HierarchyInterface $child): bool
    {
        return $this->getChildren()->contains($child);
    }
}
