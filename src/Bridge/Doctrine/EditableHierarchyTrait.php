<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine;

use WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyInterface;

/**
 * Trait EditableHierarchyTrait
 *
 * @author Benjamin Georgeault
 */
trait EditableHierarchyTrait // implements HierarchyInterface, EditableHierarchyInterface
{
    use HierarchyTrait;

    public function setParent(?EditableHierarchyInterface $parent): static
    {
        $this->parent = $parent;

        if (null !== $parent && !$parent->hasChild($this) && $parent instanceof EditableHierarchyInterface) {
            $parent->addChild($this);
        }

        return $this;
    }

    public function addChild(EditableHierarchyInterface $child): static
    {
        if ($this->hasChild($child)) {
            throw new \InvalidArgumentException(sprintf('Child already in the collection.'));
        }

        $this->getChildren()->add($child);

        if ($this !== $child->getParent() && $child instanceof EditableHierarchyInterface) {
            $child->setParent($this);
        }

        return $this;
    }

    public function remove(EditableHierarchyInterface $child): static
    {
        if (!$this->hasChild($child)) {
            throw new \OutOfBoundsException(sprintf('Child not found in the collection.'));
        }

        $this->getChildren()->removeElement($child);

        return $this;
    }
}
