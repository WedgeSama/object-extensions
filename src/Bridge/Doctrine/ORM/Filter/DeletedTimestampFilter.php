<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Filter;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use WS\Library\ObjectExtensions\Timestamp\Model\DeletedTimestampInterface;

/**
 * Class DeletedTimestampFilter
 *
 * @author Benjamin Georgeault
 */
class DeletedTimestampFilter extends SQLFilter
{
    private array $ignoredClasses = [];

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (null === $reflectionClass = $targetEntity->reflClass) {
            return '';
        }

        if ($reflectionClass->implementsInterface(DeletedTimestampInterface::class)) {
            return '';
        }

        foreach ($this->ignoredClasses as $ignoredClass) {
            if ($targetEntity->rootEntityName === $reflectionClass->getName() || $reflectionClass->isSubclassOf($ignoredClass)) {
                return '';
            }
        }

        return sprintf(
            '(%s.deleted_at IS NULL || %s.deleted_at > %s)',
            $targetTableAlias,
            $targetTableAlias,
            $this->getConnection()->getDatabasePlatform()->getCurrentTimestampSQL(),
        );
    }

    public function addIgnoredClass(string $className): static
    {
        if (!in_array($className, $this->ignoredClasses)) {
            $this->ignoredClasses[] = $className;
            $this->setParameter(sprintf('disable_%s', $className), true);
        }

        return $this;
    }
}
