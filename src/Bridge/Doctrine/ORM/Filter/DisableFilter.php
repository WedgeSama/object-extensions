<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Filter;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;

/**
 * Class DisableFilter
 *
 * @author Benjamin Georgeault
 */
class DisableFilter extends SQLFilter
{
    private array $ignoredClasses = [];

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (null === $reflectionClass = $targetEntity->reflClass) {
            return '';
        }

        if ($reflectionClass->implementsInterface(CanDisableInterface::class)) {
            return '';
        }

        foreach ($this->ignoredClasses as $ignoredClass) {
            if ($targetEntity->rootEntityName === $reflectionClass->getName() || $reflectionClass->isSubclassOf($ignoredClass)) {
                return '';
            }
        }

        return sprintf('%s.disable = 0', $targetTableAlias);
    }

    public function addIgnoredClass(string $className): static
    {
        if (!in_array($className, $this->ignoredClasses)) {
            $this->ignoredClasses[] = $className;
            $this->setParameter(sprintf('disable_%s', $className), true);
        }

        return $this;
    }
}
