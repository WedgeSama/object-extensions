<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;

/**
 * Class DisableSubscriber
 *
 * @author Benjamin Georgeault
 */
class DisableSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(CanDisableInterface::class)) {
            $this->mappingDisable($metadata);
        }
    }

    private function mappingDisable(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('disabled')) {
            $metadata->mapField([
                'fieldName' => 'disabled',
                'type' => 'boolean',
                'nullable' => false,
                'options' => [
                    'default' => false,
                ],
            ]);
        }
    }
}
