<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Class BlameSubscriber
 *
 * @author Benjamin Georgeault
 */
readonly class BlameSubscriber implements EventSubscriber
{
    /**
     * BlameSubscriber constructor.
     * @param string $userEntityClass
     * @param string[] $namespaces Use to apply this mapping only for some entity namespaces.
     */
    public function __construct(
        private string $userEntityClass,
        private array $namespaces = [],
    ) {}

    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();

        if (false === $mappingAllowed = empty($this->namespaces)) {
            foreach ($this->namespaces as $namespace) {
                if (strstr($reflectionClass->getName(), $namespace)) {
                    $mappingAllowed = true;
                    break;
                }
            }
        }

        if (true === $mappingAllowed) {
            if ($reflectionClass->implementsInterface(CreatedBlameInterface::class)) {
                $this->mappingCreatedBlame($metadata);
            }

            if ($reflectionClass->implementsInterface(UpdatedBlameInterface::class)) {
                $this->mappingUpdatedBlame($metadata);
            }
        }
    }

    public function mappingCreatedBlame(ClassMetadata $metadata): void
    {
        if (!$metadata->hasAssociation('createdBy')) {
            $metadata->mapManyToOne([
                'fieldName' => 'createdBy',
                'targetEntity' => $this->userEntityClass,
                'joinColumns' => [[
                    'referencedColumnName' => 'id', // TODO allow update of ref
                    'onDelete' => 'SET NULL',
                ]],
                'fetch' => ClassMetadata::FETCH_EAGER,
            ]);
        }
    }

    public function mappingUpdatedBlame(ClassMetadata $metadata): void
    {
        if (!$metadata->hasAssociation('updatedBy')) {
            $metadata->mapManyToOne([
                'fieldName' => 'updatedBy',
                'targetEntity' => $this->userEntityClass,
                'joinColumns' => [[
                    'referencedColumnName' => 'id', // TODO allow update of ref
                    'onDelete' => 'SET NULL',
                ]],
                'fetch' => ClassMetadata::FETCH_EAGER,
            ]);
        }
    }
}
