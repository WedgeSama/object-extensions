<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface;

/**
 * Class HierarchySubscriber
 *
 * @author Benjamin Georgeault
 */
class HierarchySubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        if ($metadata->isMappedSuperclass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(HierarchyInterface::class)) {
            $this->mappingHierarchy($metadata);
        }
    }

    private function mappingHierarchy(ClassMetadata $metadata): void
    {
        $class = $metadata->getReflectionClass()->getName();

        if (!$metadata->hasAssociation('parent') && $metadata->reflClass->hasProperty('parent')) {
            $metadata->mapManyToOne([
                'fieldName' => 'parent',
                'targetEntity' => $class,
                'inversedBy' => 'children',
                'joinColumns' => array_map(function (string $idName): array {
                    return [
                        'referencedColumnName' => $idName,
                        'onDelete' => 'SET NULL',
                    ];
                }, $metadata->getIdentifierColumnNames()),
            ]);
        }

        if (!$metadata->hasAssociation('children') && $metadata->reflClass->hasProperty('children')) {
            $metadata->mapOneToMany([
                'fieldName' => 'children',
                'targetEntity' => $class,
                'mappedBy' => 'parent',
            ]);
        }
    }
}
