<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\Timestamp\Model\CreatedTimestampInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\DeletedTimestampInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\UpdatedTimestampInterface;

/**
 * Class TimestampSubscriber
 *
 * @author Benjamin Georgeault
 */
class TimestampSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(CreatedTimestampInterface::class)) {
            $this->mappingCreatedTimestamp($metadata);
        }

        if ($reflectionClass->implementsInterface(UpdatedTimestampInterface::class)) {
            $this->mappingUpdatedTimestamp($metadata);
        }

        if ($reflectionClass->implementsInterface(DeletedTimestampInterface::class)) {
            $this->mappingDeletedTimestamp($metadata);
        }
    }

    private function mappingUpdatedTimestamp(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('updatedAt')) {
            $metadata->mapField([
                'fieldName' => 'updatedAt',
                'type' => 'datetime',
                'nullable' => true,
            ]);
        }
    }

    private function mappingCreatedTimestamp(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('createdAt')) {
            $metadata->mapField([
                'fieldName' => 'createdAt',
                'type' => 'datetime',
            ]);
        }
    }

    private function mappingDeletedTimestamp(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('deletedAt')) {
            $metadata->mapField([
                'fieldName' => 'deletedAt',
                'type' => 'datetime',
                'nullable' => true,
            ]);
        }
    }
}
