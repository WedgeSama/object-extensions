<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;
use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;

/**
 * Class SlugSubscriber
 *
 * @author Benjamin Georgeault
 */
class SlugSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(SlugInterface::class)) {
            $this->mappingSlug($metadata);
        }
        
        if ($reflectionClass->implementsInterface(HierarchySlugInterface::class)) {
            $this->mappingHierarchySlug($metadata);
        }
    }

    private function mappingSlug(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('slug')) {
            $metadata->mapField([
                'fieldName' => 'slug',
                'type' => 'string',
                'length' => 255,
            ]);
        }
    }

    private function mappingHierarchySlug(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('hierarchySlug')) {
            $metadata->mapField([
                'fieldName' => 'hierarchySlug',
                'type' => 'string',
                'length' => 4095,
            ]);
        }
    }
}
