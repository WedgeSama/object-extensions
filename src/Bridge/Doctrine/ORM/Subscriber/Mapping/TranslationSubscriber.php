<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\Translation\Exception\DoubleInterfaceException;
use WS\Library\ObjectExtensions\Translation\Exception\MissingTranslationPrefixException;
use WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslationInterface;

/**
 * Class TranslationSubscriber
 *
 * @author Benjamin Georgeault
 */
class TranslationSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        if ($metadata->isMappedSuperclass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        $both = 0;
        if ($reflectionClass->implementsInterface(TranslatableInterface::class)) {
            $this->mappingTranslatable($metadata, $reflectionClass);
            $both++;
        }

        if ($reflectionClass->implementsInterface(TranslationInterface::class)) {
            if (!preg_match('/\\\\Translation\\\\([^\\\\]+)$/', $reflectionClass->getName())) {
                throw new MissingTranslationPrefixException($reflectionClass->getName());
            }

            $this->mappingTranslation($metadata, $reflectionClass);
            $both++;
        }

        if ($both > 1) {
            throw new DoubleInterfaceException($reflectionClass->getName());
        }
    }

    private function mappingTranslatable(ClassMetadata $metadata, \ReflectionClass $reflectionClass): void
    {
        if (!$metadata->hasAssociation('translations')) {
            $metadata->mapOneToMany([
                'fieldName' => 'translations',
                'targetEntity' => $reflectionClass->getNamespaceName() . '\\Translation\\' . $reflectionClass->getShortName(),
                'mappedBy' => 'translatable',
                'indexBy' => 'locale',
                'cascade' => ['persist', 'remove'],
                'fetch' => ClassMetadata::FETCH_LAZY,
                'orphanRemoval' => true,
            ]);
        }

        if (!$metadata->hasField('fallback')) {
            $metadata->mapField([
                'fieldName' => 'fallback',
                'type' => 'string',
                'length' => 5,
            ]);
        }
    }

    private function mappingTranslation(ClassMetadata $metadata, \ReflectionClass $reflectionClass): void
    {
        if (!str_ends_with($tableName = $metadata->getTableName(), '_translation')) {
            $metadata->setPrimaryTable([
                'name' => $tableName . '_translation',
            ]);
        }

        if (!$metadata->hasField('locale')) {
            $metadata->mapField([
                'fieldName' => 'locale',
                'type' => 'string',
                'length' => 5,
            ]);
        }

        if (!$metadata->hasAssociation('translatable')) {
            $metadata->mapManyToOne([
                'fieldName' => 'translatable',
                'targetEntity' => preg_replace('/\\\\Translation\\\\([^\\\\]+)$/', '\\\\\1', $reflectionClass->getName()),
                'inversedBy' => 'translations',
                'cascade' => ['persist'],
                'fetch' => ClassMetadata::FETCH_EAGER,
                'joinColumns' => [[
                    'name' => 'translatable_id',
                    'referencedColumnName' => 'id',
                    'onDelete' => 'CASCADE',
                    'nullable' => false,
                ]],
            ]);
        }

        $metadata->table['uniqueConstraints'][$metadata->getTableName().'_translation_unique'] = [
            'columns' => ['translatable_id', 'locale'],
        ];
    }
}
