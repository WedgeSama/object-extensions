<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use WS\Library\ObjectExtensions\Blame\BlamerInterface;
use WS\Library\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Class BlameSubscriber
 *
 * @author Benjamin Georgeault
 */
readonly class BlameSubscriber implements EventSubscriber
{
    public function __construct(
        private BlamerInterface $blamer,
    ) {}

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(PrePersistEventArgs $event): void
    {
        $entity = $event->getObject();

        if ($entity instanceof CreatedBlameInterface) {
            if ($this->blamer->support(get_class($entity))) {
                $uow = $event->getObjectManager()->getUnitOfWork();
                $oldUser = $entity->getCreatedBy();
                $this->blamer->applyCreatedBy($entity);

                if ($oldUser !== $user = $entity->getCreatedBy()) {
                    $uow->propertyChanged($entity, 'createdBy', $oldUser, $user);
                    $uow->scheduleExtraUpdate($entity, [
                        'createdBy' => [$oldUser, $user],
                    ]);
                }
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $event): void
    {
        $entity = $event->getObject();

        if ($entity instanceof UpdatedBlameInterface) {
            if ($this->blamer->support(get_class($entity))) {
                $uow = $event->getObjectManager()->getUnitOfWork();
                $oldUser = $entity->getUpdatedBy();
                $this->blamer->applyUpdatedBy($entity);

                if ($oldUser !== $user = $entity->getUpdatedBy()) {
                    $uow->propertyChanged($entity, 'updatedBy', $oldUser, $user);
                    $uow->scheduleExtraUpdate($entity, [
                        'updatedBy' => [$oldUser, $user],
                    ]);
                }
            }
        }
    }
}
