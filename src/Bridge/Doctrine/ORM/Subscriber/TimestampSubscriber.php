<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use WS\Library\ObjectExtensions\Timestamp\Model\CreatedTimestampInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\DeletedTimestampInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\UpdatedTimestampInterface;

/**
 * Class TimestampSubscriber
 *
 * @author Benjamin Georgeault
 */
class TimestampSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::preRemove,
            Events::onFlush,
        ];
    }

    public function prePersist(PrePersistEventArgs $event): void
    {
        $entity = $event->getObject();

        if ($entity instanceof CreatedTimestampInterface) {
            $entity->getCreatedAt();
        }
    }

    public function preUpdate(PreUpdateEventArgs $event): void
    {
        $entity = $event->getObject();

        if ($entity instanceof UpdatedTimestampInterface) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    public function preRemove(PreRemoveEventArgs $event): void
    {
        $entity = $event->getObject();

        if ($entity instanceof DeletedTimestampInterface) {
            // If no deletedAt is set, we set it. To marked as deleted and prevent delete in onFlush.
            // Otherwise, set to null to keep delete in onFlush.
            $entity->setDeletedAt((null === $entity->getDeletedAt()) ? new \DateTime() : null);
        }
    }

    public function onFlush(OnFlushEventArgs $event): void
    {
        $om = $event->getObjectManager();
        $uow = $om->getUnitOfWork();

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof DeletedTimestampInterface) {
                if (null === $entity->getDeletedAt()) {
                    // If deletedAt null, remove the entity as usual.
                    continue;
                }

                $om->persist($entity);
                $uow->recomputeSingleEntityChangeSet($om->getClassMetadata(get_class($entity)), $entity);
            }
        }
    }
}
