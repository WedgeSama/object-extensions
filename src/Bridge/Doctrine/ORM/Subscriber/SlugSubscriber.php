<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;
use WS\Library\ObjectExtensions\Slug\Slugger;
use WS\Library\ObjectExtensions\Slug\SluggerInterface;

/**
 * Class SlugSubscriber
 *
 * @author Benjamin Georgeault
 */
readonly class SlugSubscriber implements EventSubscriber
{
    public function __construct(
        private SluggerInterface $slugger = new Slugger(),
        private bool             $onPreUpdate = true,
    ) {}

    public function getSubscribedEvents(): array
    {
        $events = [
            Events::prePersist,
        ];

        if ($this->onPreUpdate) {
            $events[] = Events::preUpdate;
        }

        return $events;
    }

    public function prePersist(PrePersistEventArgs $event): void
    {
        $this->generateSlug($event);
    }

    public function preUpdate(PreUpdateEventArgs $event): void
    {
        $this->generateSlug($event);
    }

    private function generateSlug(LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if ($entity instanceof SlugInterface) {
            $this->slugger->generateSlug($entity);
        }
    }
}
