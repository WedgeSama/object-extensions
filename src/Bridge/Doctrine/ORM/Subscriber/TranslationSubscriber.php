<?php
/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostLoadEventArgs;
use Doctrine\ORM\Events;
use WS\Library\ObjectExtensions\Translation\LocalizerInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface;

/**
 * Class TranslationSubscriber
 *
 * @author Benjamin Georgeault
 */
readonly class TranslationSubscriber implements EventSubscriber
{
    public function __construct(
        private LocalizerInterface $localizer,
    ) {}

    public function getSubscribedEvents(): array
    {
        return [
            Events::postLoad,
        ];
    }

    public function postLoad(PostLoadEventArgs $event): void
    {
        $entity = $event->getObject();
        if ($entity instanceof TranslatableInterface) {
            $entity->setCurrentLocale($this->localizer->getCurrentLocale());
            $entity->setFallbackLocale($this->localizer->getFallbackLocale());
        }
    }
}
