<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use WS\Library\ObjectExtensions\Slug\HierarchySlugger;
use WS\Library\ObjectExtensions\Slug\HierarchySluggerInterface;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Class HierarchySlugSubscriber
 *
 * @author Benjamin Georgeault
 */
readonly class HierarchySlugSubscriber implements EventSubscriber
{
    public function __construct(
        private HierarchySluggerInterface $slugger = new HierarchySlugger(),
    ) {}

    public function getSubscribedEvents(): array
    {
        return [
            Events::preFlush,
        ];
    }

    public function preFlush(PreFlushEventArgs $event): void
    {
        $em = $event->getObjectManager();
        $uow = $em->getUnitOfWork();

        $entities = array_merge($uow->getScheduledEntityInsertions(), $uow->getScheduledEntityUpdates());

        foreach ($entities as $entity) {
            if ($entity instanceof HierarchySlugInterface) {
                $this->slugger->generateHierarchySlug($entity);
                $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }
    }
}
