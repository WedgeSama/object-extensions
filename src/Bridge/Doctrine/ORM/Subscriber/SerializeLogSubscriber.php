<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostRemoveEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\SerializeLog\LoggerInterface;
use WS\Library\ObjectExtensions\SerializeLog\Model\ChildSerializeLogInterface;
use WS\Library\ObjectExtensions\SerializeLog\Model\SerializeLogInterface;

/**
 * Class SerializeLogSubscriber
 *
 * @author Benjamin Georgeault
 */
class SerializeLogSubscriber implements EventSubscriber
{
    private array $queue = [];

    /**
     * @var Array<SerializeLogInterface>
     */
    private array $parentReferences = [];

    public function __construct(
        private readonly LoggerInterface $logger,
    ) {}

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
            Events::postFlush,
        ];
    }

    public function postPersist(PostPersistEventArgs $args): void
    {
        if (null !== $entity = $this->getSerializeLogEntity($args)) {
            $this->queue[] = $this->logger->log($entity, $this->getMessage($args, $entity, 'create'));
        } elseif (null !== $childEntity = $this->getChildSerializeLogEntity($args)) {
            $parent = $childEntity->getParentForSerializeLog();
            $this->queue[] = $this->logger->log($parent, $this->getMessage($args, $parent, 'from child'));
        }
    }

    public function postUpdate(PostUpdateEventArgs $args): void
    {
        if (null !== $entity = $this->getSerializeLogEntity($args)) {
            $this->queue[] = $this->logger->log($entity, $this->getMessage($args, $entity, 'update'));
        } elseif (null !== $childEntity = $this->getChildSerializeLogEntity($args)) {
            $parent = $childEntity->getParentForSerializeLog();
            $this->queue[] = $this->logger->log($parent, $this->getMessage($args, $parent, 'from child'));
        }
    }

    public function postRemove(PostRemoveEventArgs $args): void
    {
        if (null !== $entity = $this->getSerializeLogEntity($args)) {
            $this->queue[] = $this->logger->log($entity, $this->getMessage($args, $entity, 'remove'));
        } elseif (null !== $childEntity = $this->getChildSerializeLogEntity($args)) {
            $parent = $childEntity->getParentForSerializeLog();
            $this->queue[] = $this->logger->log($parent, $this->getMessage($args, $parent, 'from child'));
        }
    }

    public function postFlush(PostFlushEventArgs $args): void
    {
        if (!empty($this->queue)) {
            $em = $args->getObjectManager();

            foreach ($this->queue as $entity) {
                $em->persist($entity);
            }

            $this->queue = [];
            $em->flush();
        }

        $this->parentReferences = [];
    }

    private function getSerializeLogEntity(LifecycleEventArgs $args): ?SerializeLogInterface
    {
        $entity = $args->getObject();
        if ($entity instanceof SerializeLogInterface) {
            $this->logger->setEm($args->getObjectManager());
            return $entity;
        }

        return null;
    }

    private function getChildSerializeLogEntity(LifecycleEventArgs $args): ?ChildSerializeLogInterface
    {
        $entity = $args->getObject();
        if (
            $entity instanceof ChildSerializeLogInterface
            && (null !== $parent = $entity->getParentForSerializeLog())
            && !in_array($parent, $this->parentReferences)
        ) {
            $this->logger->setEm($args->getObjectManager());
            $this->parentReferences[] = $parent;
            return $entity;
        }

        return null;
    }

    private function getMessage(LifecycleEventArgs $args, $entity, string $messageSuffix = ''): string
    {
        $metadata = $args->getObjectManager()->getClassMetadata(get_class($entity));
        return sprintf(
            'class: %s id: %s - %s',
            $metadata->getName(),
            json_encode($metadata->getIdentifierValues($entity)),
            $messageSuffix
        );
    }
}
