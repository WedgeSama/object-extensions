<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine\ORM\SerializeLog;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\ObjectExtensions\SerializeLog\AbstractLogger;
use WS\Library\ObjectExtensions\SerializeLog\Model\SerializeLogInterface;

/**
 * Class ORMLogger
 *
 * @author Benjamin Georgeault
 */
class ORMLogger extends AbstractLogger implements LoggerInterface
{
    private EntityManagerInterface $em;

    public function setEm(EntityManagerInterface $em): void
    {
        $this->em = $em;
    }

    protected function getClass(SerializeLogInterface $object): string
    {
        return $this->getMetadata($object)->getName();
    }

    protected function getIdentifier(SerializeLogInterface $object): array
    {
        return $this->getMetadata($object)->getIdentifierValues($object);
    }

    private function getMetadata(SerializeLogInterface $object): ClassMetadata
    {
        return $this->em->getMetadataFactory()->getMetadataFor(get_class($object));
    }
}
