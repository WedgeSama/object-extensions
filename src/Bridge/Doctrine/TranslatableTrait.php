<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Bridge\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use WS\Library\ObjectExtensions\Translation\Exception\AlreadyExistTranslationException;
use WS\Library\ObjectExtensions\Translation\Exception\NotFoundTranslationException;
use WS\Library\ObjectExtensions\Translation\Model\TranslationInterface;

/**
 * Trait TranslatableTrait
 *
 * For usage with
 *
 * @author Benjamin Georgeault
 * @see \WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface
 */
trait TranslatableTrait // implements TranslatableInterface
{
    /**
     * @var Collection<TranslationInterface>
     */
    protected ?Collection $translations = null;

    /**
     * @deprecated 2.1.0 Use {@see getFallbackLocale()} instead.
     */
    protected ?string $fallback = null;

    private ?string $currentLocale = null;

    /**
     * @return Collection<TranslationInterface>
     */
    public function getTranslations(): Collection
    {
        return $this->translations ? : $this->translations = new ArrayCollection();
    }

    /**
     * @throws NotFoundTranslationException
     */
    public function getTranslation(?string $locale = null, ?string $fallback = null): TranslationInterface
    {
        $locale = $locale ?? $this->getCurrentLocale();

        if ($this->hasTranslation($locale)) {
            return $this->getTranslations()->get($locale);
        }

        if (null === $fallback) {
            $fallback = $this->getFallback();
        }

        if (null !== $fallback && $this->hasTranslation($fallback)) {
            return $this->getTranslations()->get($fallback);
        }

        throw new NotFoundTranslationException($this, $locale);
    }

    public function getTranslationOrNull(?string $locale = null, ?string $fallback = null): ?TranslationInterface
    {
        try {
            return $this->getTranslation($locale, $fallback);
        } catch (NotFoundTranslationException) {
            return null;
        }
    }

    public function hasTranslation(string $locale): bool
    {
        return $this->getTranslations()->containsKey($locale);
    }

    /**
     * @throws AlreadyExistTranslationException
     */
    public function addTranslation(TranslationInterface $translation): static
    {
        if ($this->hasTranslation($locale = $translation->getLocale())) {
            throw new AlreadyExistTranslationException($this, $locale);
        }

        $translation->setTranslatable($this);
        $this->getTranslations()->set($locale, $translation);

        return $this;
    }

    public function getFallback(): ?string
    {
        return $this->getFallbackLocale();
    }

    public function setFallback(string $fallback): static
    {
        return $this->setFallbackLocale($fallback);
    }

    public function getFallbackLocale(): ?string
    {
        return $this->fallback;
    }

    public function setFallbackLocale(string $fallback): static
    {
        $this->fallback = $fallback;

        return $this;
    }

    public function getCurrentLocale(): ?string
    {
        return $this->currentLocale;
    }

    public function setCurrentLocale(string $locale): static
    {
        $this->currentLocale = $locale;

        return $this;
    }
}
