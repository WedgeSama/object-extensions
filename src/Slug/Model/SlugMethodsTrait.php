<?php

namespace WS\Library\ObjectExtensions\Slug\Model;

/**
 * Trait SlugMethodTrait
 *
 * @author Kévin Tourret
 * @see SlugInterface
 */
trait SlugMethodsTrait
{
    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

}