<?php

namespace WS\Library\ObjectExtensions\Slug\Model;

/**
 * Trait SlugPropertyTrait
 *
 * @author Kévin Tourret
 * @see SlugInterface
 */
trait SlugPropertyTrait
{
    protected string $slug = '';
}