<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug\Model;

use WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface;

/**
 * Interface HierarchySlugInterface
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of HierarchySlugInterface
 * @template-implements HierarchyInterface<TKey, TValue>
 */
interface HierarchySlugInterface extends HierarchyInterface, SlugInterface
{
    public function getHierarchySlug(): string;

    public function setHierarchySlug(string $hierarchySlug): static;
}
