<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug\Model;

/**
 * Interface SlugInterface
 *
 * @author Benjamin Georgeault
 */
interface SlugInterface
{
    public function getSlug(): string;

    public function setSlug(string $slug): static;

    /**
     * @return Array<string>
     */
    public static function getSlugFields(): array;
}
