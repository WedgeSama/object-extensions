<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug\Model;

/**
 * Trait HierarchySlugTrait
 *
 * @author Benjamin Georgeault
 * @see HierarchySlugInterface
 * @see \WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface
 */
trait HierarchySlugTrait // implements HierarchySlugInterface, \WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface
{
    use SlugTrait;

    protected string $hierarchySlug = '';

    public function getHierarchySlug(): string
    {
        return $this->hierarchySlug;
    }

    public function setHierarchySlug(string $hierarchySlug): static
    {
        $this->hierarchySlug = $hierarchySlug;

        return $this;
    }
}
