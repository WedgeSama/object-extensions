<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug\Exception;

/**
 * Class SlugException
 *
 * @author Benjamin Georgeault
 */
class SlugException extends \RuntimeException
{
}
