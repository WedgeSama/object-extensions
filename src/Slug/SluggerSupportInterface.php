<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug;

use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;

/**
 * Interface SluggerSupportInterface.
 * Will be merged into SluggerInterface interface in next major version.
 *
 * @author Benjamin Georgeault
 */
interface SluggerSupportInterface
{
    public function supports(SlugInterface $object): bool;
}
