<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug;

use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Interface HierarchySluggerSupportInterface
 *  Will be merged into HierarchySluggerInterface interface in next major version.
 *
 * @author Benjamin Georgeault
 */
interface HierarchySluggerSupportInterface
{
    public function supports(HierarchySlugInterface $object): bool;
}
