<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug;

use WS\Library\ObjectExtensions\Slug\Exception\SlugException;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Interface HierarchySluggerInterface
 *
 * @author Benjamin Georgeault
 */
interface HierarchySluggerInterface
{
    /**
     * @throws SlugException
     */
    public function generateHierarchySlug(HierarchySlugInterface $hierarchySlugObject): void;
}
