<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug;

use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Class HierarchySlugger
 *
 * @author Benjamin Georgeault
 */
readonly class HierarchySlugger implements HierarchySluggerInterface
{
    public function __construct(
        private string $delimiter = '/',
    ) {}

    public function generateHierarchySlug(HierarchySlugInterface $hierarchySlugObject): void
    {
        $slug = '';
        $current = $hierarchySlugObject;

        do {
            $slug = $current->getSlug().$this->delimiter.$slug;
        } while (null !== $current = $current->getParent());

        $hierarchySlugObject->setHierarchySlug('/'.trim($slug, '/'));
    }
}
