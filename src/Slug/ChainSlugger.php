<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug;

use WS\Library\ObjectExtensions\Slug\Exception\SluggerNotFoundException;
use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;

/**
 * Class ChainSlugger
 *
 * @author Benjamin Georgeault
 */
readonly class ChainSlugger implements SluggerInterface, SluggerSupportInterface
{
    public function __construct(
        /** @var iterable<SluggerInterface&SluggerSupportInterface> */
        private iterable $sluggers,
    ) {}

    public function generateSlug(SlugInterface $slugObject): void
    {
        foreach ($this->getSluggers() as $slugger) {
            if ($slugger->supports($slugObject)) {
                $slugger->generateSlug($slugObject);
                return;
            }
        }

        throw new SluggerNotFoundException(sprintf('No slugger found for object of class %s', get_class($slugObject)));
    }

    public function supports(SlugInterface $object): bool
    {
        foreach ($this->getSluggers() as $slugger) {
            if ($slugger->supports($object)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return iterable<SluggerInterface&SluggerSupportInterface>
     */
    private function getSluggers(): iterable
    {
        foreach ($this->sluggers as $slugger) {
            if (!$slugger instanceof SluggerInterface) {
                throw new \InvalidArgumentException(sprintf('Slugger must implement %s', SluggerInterface::class));
            }

            if (!$slugger instanceof SluggerSupportInterface) {
                throw new \InvalidArgumentException(sprintf('Slugger must implement %s', SluggerSupportInterface::class));
            }

            yield $slugger;
        }
    }
}
