<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Slug;

use WS\Library\ObjectExtensions\Slug\Exception\SluggerNotFoundException;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Class ChainHierarchySlugger
 *
 * @author Benjamin Georgeault
 */
readonly class ChainHierarchySlugger implements HierarchySluggerInterface, HierarchySluggerSupportInterface
{
    public function __construct(
        /** @var iterable<HierarchySluggerInterface&HierarchySluggerSupportInterface> */
        private iterable $sluggers,
    ) {}

    public function generateHierarchySlug(HierarchySlugInterface $hierarchySlugObject): void
    {
        foreach ($this->getSluggers() as $slugger) {
            if ($slugger->supports($hierarchySlugObject)) {
                $slugger->generateHierarchySlug($hierarchySlugObject);
                return;
            }
        }

        throw new SluggerNotFoundException(sprintf('No hierarchy slugger found for object of class %s', get_class($hierarchySlugObject)));
    }

    public function supports(HierarchySlugInterface $object): bool
    {
        foreach ($this->getSluggers() as $slugger) {
            if ($slugger->supports($object)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return iterable<HierarchySluggerInterface&HierarchySluggerSupportInterface>
     */
    private function getSluggers(): iterable
    {
        foreach ($this->sluggers as $slugger) {
            if (!$slugger instanceof HierarchySluggerInterface) {
                throw new \InvalidArgumentException(sprintf('Slugger must implement %s', HierarchySluggerInterface::class));
            }

            if (!$slugger instanceof HierarchySluggerSupportInterface) {
                throw new \InvalidArgumentException(sprintf('Slugger must implement %s', HierarchySluggerSupportInterface::class));
            }

            yield $slugger;
        }
    }
}
