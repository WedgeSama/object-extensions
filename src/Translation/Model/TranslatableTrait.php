<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Model;

use WS\Library\ObjectExtensions\Translation\Exception\AlreadyExistTranslationException;
use WS\Library\ObjectExtensions\Translation\Exception\NotFoundTranslationException;

/**
 * Trait TranslatableTrait
 *
 * @author Benjamin Georgeault
 * @see \WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface
 */
trait TranslatableTrait // implements TranslatableInterface
{
    /**
     * @var iterable<TranslationInterface>
     */
    protected iterable $translations;

    /**
     * @deprecated 2.1.0 Use {@see getFallbackLocale()} instead.
     */
    protected ?string $fallback = null;

    private ?string $currentLocale = null;

    /**
     * @return iterable<TranslationInterface>
     */
    public function getTranslations(): iterable
    {
        return $this->translations ? : $this->translations = [];
    }

    /**
     * @throws NotFoundTranslationException
     */
    public function getTranslation(string $locale = null, ?string $fallback = null): TranslationInterface
    {
        $locale = $locale ?? $this->getCurrentLocale();

        if ($this->hasTranslation($locale)) {
            return $this->getTranslations()[$locale];
        }

        if (null === $fallback) {
            $fallback = $this->getFallback();
        }

        if (null !== $fallback && $this->hasTranslation($fallback)) {
            return $this->getTranslations()[$fallback];
        }

        throw new NotFoundTranslationException($this, $locale);
    }

    public function getTranslationOrNull(string $locale = null, ?string $fallback = null): ?TranslationInterface
    {
        try {
            return $this->getTranslation($locale, $fallback);
        } catch (NotFoundTranslationException $e) {
            return null;
        }
    }

    public function hasTranslation(string $locale): bool
    {
        return array_key_exists($locale, $this->getTranslations());
    }

    /**
     * @throws AlreadyExistTranslationException
     */
    public function addTranslation(TranslationInterface $translation): static
    {
        if ($this->hasTranslation($locale = $translation->getLocale())) {
            throw new AlreadyExistTranslationException($this, $locale);
        }

        $translation->setTranslatable($this);
        $this->getTranslations();
        $this->translations[$locale] = $translation;

        return $this;
    }

    public function getFallback(): ?string
    {
        return $this->getFallbackLocale();
    }

    public function setFallback(string $fallback): static
    {
        return $this->setFallbackLocale($fallback);
    }

    public function getFallbackLocale(): ?string
    {
        return $this->fallback;
    }

    public function setFallbackLocale(string $fallback): static
    {
        $this->fallback = $fallback;

        return $this;
    }

    public function getCurrentLocale(): ?string
    {
        return $this->currentLocale;
    }

    public function setCurrentLocale(string $locale): static
    {
        $this->currentLocale = $locale;

        return $this;
    }
}
