<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Model;

/**
 * Trait TranslationTrait
 *
 * @author Benjamin Georgeault
 * @see TranslationInterface
 */
trait TranslationTrait // implements TranslationInterface
{
    protected string $locale = 'en';

    protected ?TranslatableInterface $translatable;

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): static
    {
        $this->locale = $locale;

        return $this;
    }

    public function getTranslatable(): ?TranslatableInterface
    {
        return $this->translatable;
    }

    public function setTranslatable(TranslatableInterface $translatable): static
    {
        $this->translatable = $translatable;

        return $this;
    }
}
