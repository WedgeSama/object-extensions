<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Model;

use WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface;

/**
 * Trait HierarchyTranslationTrait
 *
 * @author Benjamin Georgeault
 */
trait HierarchyTranslationTrait
{
    public function getIterator(): \Traversable
    {
        foreach ($this->getTranslatable()->getTranslations() as $translation) {
            yield $translation;
        }
    }

    public function count(): int
    {
        return iterator_count($this->getChildren());
    }

    public function getParent(): ?HierarchyInterface
    {
        return $this->getTranslatable()
            ?->getParent()
            ?->getTranslationOrNull($this->getLocale())
            ;
    }

    public function getChildren(): iterable
    {
        foreach ($this->getTranslatable()->getChildren() as $child) {
            if (null !== $translation = $child->getTranslationOrNull($this->getLocale())) {
                yield $translation;
            }
        }
    }

    public function hasChild(HierarchyInterface $child): bool
    {
        $children = $this->getChildren();

        if (is_array($children)) {
            return in_array($child, $children);
        }

        foreach ($children as $translation) {
            if ($child === $translation) {
                return true;
            }
        }

        return false;
    }
}
