<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Model;

use WS\Library\ObjectExtensions\Translation\Exception\AlreadyExistTranslationException;
use WS\Library\ObjectExtensions\Translation\Exception\NotFoundTranslationException;

/**
 * Object that has to be translatable has to implement this interface.
 *
 * @author Benjamin Georgeault
 *
 * @template T of TranslationInterface
 */
interface TranslatableInterface
{
    /**
     * Get all Translations for this Translatable.
     *
     * @psalm-return iterable<T>
     */
    public function getTranslations(): iterable;

    /**
     * Get a Translation by its locale.
     *
     * @throws NotFoundTranslationException
     * @psalm-return T
     */
    public function getTranslation(string $locale = null, ?string $fallback = null): TranslationInterface;

    /**
     * @psalm-return T|null
     */
    public function getTranslationOrNull(string $locale = null, ?string $fallback = null): ?TranslationInterface;

    /**
     * Check if the given locale exist for this Translatable.
     */
    public function hasTranslation(string $locale): bool;

    /**
     * Add a Translation.
     *
     * @throws AlreadyExistTranslationException
     *
     * @psalm-param T $translation
     */
    public function addTranslation(TranslationInterface $translation): static;

    /**
     * @deprecated 2.1.0 Use {@see getFallbackLocale()} instead.
     */
    public function getFallback(): ?string;

    /**
     * @deprecated 2.1.0 Use {@see setFallbackLocale()} instead.
     */
    public function setFallback(string $fallback): static;

    public function getFallbackLocale(): ?string;

    public function setFallbackLocale(string $fallback): static;

    public function getCurrentLocale(): ?string;

    public function setCurrentLocale(string $locale): static;
}
