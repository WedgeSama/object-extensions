<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Model;

/**
 * Interface TranslationInterface
 *
 * @author Benjamin Georgeault
 *
 * @template T of TranslatableInterface
 */
interface TranslationInterface
{
    public function getLocale(): string;

    public function setLocale(string $locale): static;

    /**
     * @psalm-return T|null
     */
    public function getTranslatable(): ?TranslatableInterface;

    /**
     * @psalm-param T $translatable
     */
    public function setTranslatable(TranslatableInterface $translatable): static;
}
