<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Exception;

use WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslationInterface;

/**
 * Class DoubleInterfaceException
 *
 * @author Benjamin Georgeault
 */
class DoubleInterfaceException extends \LogicException
{
    public function __construct(string $class)
    {
        parent::__construct(sprintf(
            'The class "%s" cannot implement both "%s" and "%s" interfaces. Choose only one.',
            $class,
            TranslationInterface::class,
            TranslatableInterface::class
        ));
    }
}
