<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Exception;

use WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface;

/**
 * Class NotFoundTranslationException
 *
 * @author Benjamin Georgeault
 */
class NotFoundTranslationException extends \RuntimeException
{
    public function __construct(
        private readonly TranslatableInterface $translatable,
        private readonly string $locale,
    ) {
        parent::__construct(sprintf('Translation for locale "%s" not found.', $locale));
    }

    public function getTranslatable(): TranslatableInterface
    {
        return $this->translatable;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }
}
