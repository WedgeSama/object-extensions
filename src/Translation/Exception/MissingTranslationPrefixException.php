<?php

/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation\Exception;

/**
 * Class MissingTranslationPrefixException
 *
 * @author Benjamin Georgeault
 */
class MissingTranslationPrefixException extends \LogicException
{
    public function __construct(string $class)
    {
        parent::__construct(sprintf('The class "%s" missing Translation prefix directory.', $class));
    }
}
