<?php
/*
 * This file is part of the wedgesama/object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\ObjectExtensions\Translation;

/**
 * Interface LocalizerInterface
 *
 * @author Benjamin Georgeault
 */
interface LocalizerInterface
{
    /**
     * Return the current locale asked by the user.
     */
    public function getCurrentLocale(): string;

    /**
     * Return the fallback (default) locale.
     */
    public function getFallbackLocale(): string;
}
