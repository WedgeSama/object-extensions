<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\ObjectExtensions\Timestamp;

use PHPUnit\Framework\TestCase;
use WS\Tests\Library\ObjectExtensions\Fixtures\Timestamp\Model\Article;

/**
 * Class TimestampTest
 *
 * @author Benjamin Georgeault
 */
class TimestampTest extends TestCase
{
    public function testGetCreatedAtDefault(): void
    {
        $article = new Article();
        $this->assertNotNull($article->getCreatedAt());
    }

    public function testSetCreatedAt(): void
    {
        $article = new Article();

        $article->setCreatedAt(new \DateTime('2021-01-01 00:00:00'));
        $this->assertEquals('2021-01-01 00:00:00', $article->getCreatedAt()->format('Y-m-d H:i:s'));
    }

    public function testGetUpdatedAtDefault(): void
    {
        $article = new Article();
        $this->assertNull($article->getUpdatedAt());
    }

    public function testSetUpdatedAt(): void
    {
        $article = new Article();

        $article->setUpdatedAt(new \DateTime('2021-01-01 00:00:00'));
        $this->assertEquals('2021-01-01 00:00:00', $article->getUpdatedAt()->format('Y-m-d H:i:s'));
    }
}
