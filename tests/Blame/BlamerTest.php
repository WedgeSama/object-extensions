<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\ObjectExtensions\Blame;

use PHPUnit\Framework\TestCase;
use WS\Library\ObjectExtensions\Blame\Blamer;
use WS\Library\ObjectExtensions\Blame\BlamerInterface;
use WS\Tests\Library\ObjectExtensions\Fixtures\Blame\Model\Article;
use WS\Tests\Library\ObjectExtensions\Fixtures\Blame\Model\User;

/**
 * Class BlamerTest
 *
 * @author Benjamin Georgeault
 */
class BlamerTest extends TestCase
{
    public function testApplyCreatedBy(): void
    {
        $this->getBlamer()->applyCreatedBy($article = new Article());

        $this->assertSame('Foo Bar', $article->getCreatedBy()->getName());
    }

    public function testApplyUpdatedBy(): void
    {
        $this->getBlamer()->applyUpdatedBy($article = new Article());

        $this->assertSame('Foo Bar', $article->getUpdatedBy()->getName());
    }

    public function testSupport(): void
    {
        $this->assertTrue($this->getBlamer()->support(User::class));
        $this->assertFalse($this->getBlamer()->support(Article::class));
    }

    private function getBlamer(): BlamerInterface
    {
        return new Blamer((new User())
            ->setName('Foo Bar')
        );
    }
}
