<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\ObjectExtensions\Fixtures\Timestamp\Model;

use WS\Library\ObjectExtensions\Timestamp\Model\TimestampInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\TimestampTrait;

/**
 * Class Article
 *
 * @author Benjamin Georgeault
 */
class Article implements TimestampInterface
{
    use TimestampTrait;
}
