<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\ObjectExtensions\Fixtures\Blame\Model;

use WS\Library\ObjectExtensions\Blame\Model\BlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\BlameTrait;

/**
 * Class Article
 *
 * @author Benjamin Georgeault
 */
class Article implements BlameInterface
{
    use BlameTrait;
}
