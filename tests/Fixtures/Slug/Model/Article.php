<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\ObjectExtensions\Fixtures\Slug\Model;

use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;
use WS\Library\ObjectExtensions\Slug\Model\SlugTrait;

/**
 * Class Article
 *
 * @author Benjamin Georgeault
 */
class Article implements SlugInterface
{
    use SlugTrait;

    private ?string $title = null;

    public static function getSlugFields(): array
    {
        return [
            'title',
        ];
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }
}
