<?php
/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\ObjectExtensions\Slug;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use WS\Library\ObjectExtensions\Slug\Exception\SlugException;
use WS\Library\ObjectExtensions\Slug\Slugger;
use WS\Tests\Library\ObjectExtensions\Fixtures\Slug\Model\Article;

/**
 * Class SluggerTest
 *
 * @author Benjamin Georgeault
 */
class SluggerTest extends TestCase
{
    public function testGenerateSlugWithDefault()
    {
        $slugger = new Slugger();
        $article = (new Article())
            ->setTitle('Hello World')
        ;

        $slugger->generateSlug($article);

        $this->assertEquals('hello-world', $article->getSlug());
    }

    public function testGenerateSlugEmpty()
    {
        $slugger = new Slugger();
        $article = new Article();

        $this->expectException(SlugException::class);

        $slugger->generateSlug($article);
    }

    public function testGenerateSlugEntropy()
    {
        $slugger = new Slugger(entropy: true);
        $article = (new Article())
            ->setTitle('Hello World')
        ;

        $slugger->generateSlug($article);

        $this->assertMatchesRegularExpression('/^hello-world-[a-z0-9]{5}$/', $article->getSlug());
    }

    #[DataProvider('provideGenerateSlugWithCustomDelimiter')]
    public function testGenerateSlugWithCustomDelimiter(string $title, string $delimiter, string $expected)
    {
        $slugger = new Slugger($delimiter);
        $article = (new Article())
            ->setTitle($title)
        ;

        $slugger->generateSlug($article);

        $this->assertEquals($expected, $article->getSlug());
    }

    public static function provideGenerateSlugWithCustomDelimiter(): array
    {
        return [
            ['Hello World', '_', 'hello_world'],
            ['Hello World', '.', 'hello.world'],
            ['àáâãä ç èéêë ìíîï ñ òóôõö ùúûü ýÿ ÀÁÂÃÄ Ç ÈÉÊË ÌÍÎÏ Ñ ÒÓÔÕÖ ÙÚÛÜ Ý', '-', 'aaaaa-c-eeee-iiii-n-ooooo-uuuu-yy-aaaaa-c-eeee-iiii-n-ooooo-uuuu-y'],
            ['Hell-o World', '-', 'hell-o-world'],
            ['Hell-o World', '.', 'hell.o.world'],
            ['Hell/o/World', '.', 'hell.o.world'],
        ];
    }
}
