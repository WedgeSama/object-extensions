Slug
====

Allow an object to have a normalize slug. Useful for url's path.

## Usage with Symfony integration

### Config references

You have to register your Users classes and the list of namespaces (or only one) for which entities to apply.

```yaml
ws_object_extensions:
    slug:
        enable: true # Default at true
        delimiter: '-' # Default at -
        on_pre_update: false # Default at false
        hierarchy_delimiter: '/' # Default at /
```

### Usage in Doctrine entity

```php
<?php
namespace App\Entity;

use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;
use WS\Library\ObjectExtensions\Slug\Model\SlugTrait;

class MyEntity implements SlugInterface
{
    use SlugTrait;
    
    /** @var string */
    private $title;
    // ...
    public static function getSlugFields(): array
    {
        return [
            'title',
        ];
    }
}
```

The mapping will be automatically added to your entity. It will use the attribute 'title' to generate the slug.

### Usage with Hierarchy with Doctrine

Enable hierarchy if disabled (enable by default).
```yaml
ws_object_extensions:
    hierarchy:
        enable: true
```

Add hierarchy slug to your slugged Entity.

```php
<?php
namespace App\Entity;

use WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyInterface;
use WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyTrait;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugTrait;
use WS\Library\ObjectExtensions\Slug\Model\SlugInterface;
use WS\Library\ObjectExtensions\Slug\Model\SlugTrait;

class MyEntity implements SlugInterface, HierarchySlugInterface, EditableHierarchyInterface
{
    use SlugTrait;
    use HierarchySlugTrait;
    use EditableHierarchyTrait;
    
    /** @var string */
    private $title;
    // ...
    public static function getSlugFields(): array
    {
        return [
            'title',
        ];
    }
}
```
