Timestamp
=========

Timestamp capability allow an object have a time information for create and update.

## Usage with Symfony integration

### Config references

You have to register your Users classes and the list of namespaces (or only one) for which entities to apply.

```yaml
ws_object_extensions:
    timestamp:
        enable: true # Default at true
```

### Usage in Doctrine entity

```php
<?php
namespace App\Entity;

use WS\Library\ObjectExtensions\Timestamp\Model\TimestampInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\TimestampTrait;

class MyEntity implements TimestampInterface
{
    use TimestampTrait;
    // ...
}
```

The mapping will be automatically added to your entity.
