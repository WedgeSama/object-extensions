Hierarchy
=========

Hierarchy capability allow to create a hierarchy for a given class on itself (and children classes).

## Usage with Symfony integration

### Config references

There is no option for hierarchy at the moment.

```yaml
ws_object_extensions:
    hierarchy:
        enable: true # Default at true
```

### Usage in Doctrine entity

```php
<?php
namespace App\Entity;

use WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyInterface;
use WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyTrait;

class MyEntity implements EditableHierarchyInterface
{
    use EditableHierarchyTrait;
    // ...
}
```

The mapping will bee automatically add to the entity. 

### Others traits and interfaces

#### Editable without Doctrine
- `WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyTrait`

#### Read only hierarchy
- `WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyInterface`
- `WS\Library\ObjectExtensions\Hierarchy\Model\HierarchyTrait`

#### Read only hierarchy with Doctrine
- `WS\Library\ObjectExtensions\Bridge\Doctrine\HierarchyTrait`
