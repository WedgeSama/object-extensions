Blame
=====

Blame capability allow an object have a User reference for create and update.

## Usage with Symfony integration

### Config references

You have to register your Users classes and the list of namespaces (or only one) for which entities to apply.

```yaml
ws_object_extensions:
    blame:
        enabled: true # Default at true
        users:
            Your\User\FullQualify\ClassName: 'App\Entity'
            Another\User\FullQualify\ClassName:
                - 'App\Model\Sub'
                - 'Can\BeAn\ArrayOf\Namespace'
```

### Usage in Doctrine entity

```php
<?php
namespace App\Entity;

use WS\Library\ObjectExtensions\Blame\Model\BlameInterface;
use WS\Library\ObjectExtensions\Blame\Model\BlameTrait;
use Full\QualifyClass\Name\User; // Replace by your own User class.

/**
 * @method User getCreatedBy()
 * @method User|null getUpdatedBy()
 * @method MyEntity setCreatedBy(User $createdBy)
 * @method MyEntity setUpdatedBy(User $updatedBy)
 */
class MyEntity implements BlameInterface
{
    use BlameTrait;
    // ...
}
```

The mapping will be automatically added to your entity.
