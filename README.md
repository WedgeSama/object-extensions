Object Extensions
=================

A library to extend Object capabilities.

## Install

```
composer require wedgesama/object-extensions
```

### Symfony integration (^5.4|^6.0)

Flex not yet available.

- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    WS\Library\ObjectExtensions\Bridge\Symfony\WSObjectExtensionsBundle::class => ['all' => true],
    //...
];
```

- Create a new `config\packages\ws_extensions.yaml` file, you will config the bundle here.
```yaml
ws_object_extensions: ~
```

- That all for default configuration.

## Documentations

Here all capabilities documentations:
- [Blame](doc/blame.md): Allow an object to have a User referenced for creation and update on the object.
- [Hierarchy](doc/hierarchy.md): Allow to create a hierarchy for a given class on itself (and children classes).
- [Slug](doc/slug.md): Allow an object to have a normalize slug. Useful for url's path.
- [Timestamp](doc/timestamp.md): Same as Blame but for time.
- [Translation](doc/translation.md): Allow an object to have translation attributes.

## TODO

- Better documentation.
- Flex recipe for Symfony.
- Add example for standalone usage.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
